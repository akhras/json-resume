$(window).bind("load", function () {
    $('.loading').fadeOut();
    var people = [];
    $.getJSON('resume.json', function (data) {

        var headerInfo = '<div id="header">' + '<img id="picture" src="' + data.info.picture + '"><h1 id="name">' + data.info.name + '</h1>' + '<h3 id="title">' + data.info.title + '</h3></div>';
        $('#infoSection').append(headerInfo);

        var contactInfo = '<div id="header">' + '<div id="contact"><div id="phone"> Phone: ' + data.info.phone + '</div><div id="email">Email: ' + data.info.email + '</div><div id="address"> Address: ' + data.info.address + '</div><div id="website"> Website: ' + data.info.website + '</div>' + '</div><div id="about">' + data.info.about + '</div>';
        $('#infoSection').append(contactInfo);

        $.each(data.education, function (i, record) {
            var educationSection = '<div class="institution">' + record.institution + '</div>' + '<div id="field">' + record.field + '</div><div class="date-range">' + record.from + ' - ' + record.to + '</div><div id="gpa">' + record.gpa + '</div><hr>';
            $("#educationSection").append(educationSection);

        });

        $.each(data.experience, function (i, record) {
            var experienceSection = '<div class="position">' + record.position + '</div>' + '<div id="company">' + record.company + '</div><div class="date-range">' + record.from + ' - ' + record.to + '</div><div id="summary">' + record.summary + '</div><hr>';
            $("#experienceSection").append(experienceSection);

        });

        $.each(data.projects, function (i, record) {
            var projectsSection = '<a class="projectName" href="' + record.showcase + '">' + record.name + '</a>' + '<div class="role"><b>Role:</b> ' + record.role + '</div><div id="technologies"><b>Technologies:</b> ' + record.technologies + '</div><div id="overview"><b>Overview:</b> ' + record.overview + '</div><div id="tech"><b>Tech:</b> ' + record.tech + '</div><hr>';
            $("#projectsSection").append(projectsSection);

        });

        $.each(data.languages, function (i, language) {
            var languagesSection = '<div class="language"><span class="languageName">' + language.name + '</span>' +
                '<span class="languageLevel">' + language.level + '</span></div>';
            $("#languagesSection").append(languagesSection);

        });

        $.each(data.skills, function (i, skill) {
            var skillsSection = '<div class="category"><h1>' + skill.category + '</h1>';
            $.each(skill.technology, function (i, tech) {
                var technology = '<div class="technology"><span class="techName">' + tech.name + '</span><span class=techLevel>' + tech.level + '</span></div>';
                skillsSection += technology;
            });
            skillsSection += '</div>';
            $("#skillsSection").append(skillsSection);
        });
    });
});